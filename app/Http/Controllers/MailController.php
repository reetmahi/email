<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mail\TestMail;
use Illuminate\Support\Facades\Mail;
use App\maildata;



class MailController extends Controller
{

    public function basicMail(Request $request){
            $obj = new maildata();
            $obj->name = 'test name';
            $obj->phone = '8427981281';
            $obj->email = 'email@gmail.com';
            
            Mail::to('vaibhav.anchal16@gmail.com')->send(new TestMail($obj));
    }

}
